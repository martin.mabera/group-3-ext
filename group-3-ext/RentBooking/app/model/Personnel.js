Ext.define('RentBooking.model.Personnel', {
    extend: 'RentBooking.model.Base',

    fields: [
        'name', 'email', 'phone'
    ]
});
