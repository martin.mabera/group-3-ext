Ext.define('RentBooking.model.Tenant', {
    extend: 'Ext.data.Model',

    fields: [
        { name: 'id', type: 'int' },
        { name: 'tenantName', type: 'string' },
        { name: 'tenantEmail', type: 'string' },
        { name: 'nationalId', type: 'string' },
        { name: 'phoneNumber', type: 'string' },
        { name: 'accountName', type: 'string' },
        { name: 'accountNumber', type: 'string' },
        { name: 'taxPin', type: 'string' },
        { name: 'deleted', type: 'string' }
    ]
});
