Ext.define('RentBooking.model.Booking', {
    extend: 'Ext.data.Model',

    fields: [
        { name: 'id', type: 'int' },
        { name: 'bookingDate', type: 'date', dateFormat: 'Y-m-d H:i:s' },
        { name: 'bookingDescription', type: 'string' },
        { name: 'contractId', type: 'int' },
        { name: 'month', type: 'string' },
        { name: 'year', type: 'int' },
        { name: 'propertyId', type: 'int' }
    ]
});
