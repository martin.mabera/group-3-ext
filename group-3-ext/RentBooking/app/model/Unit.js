Ext.define('RentBooking.model.Unit', {
    extend: 'RentBooking.model.Base',

    fields: [
        { name: 'id', type: 'int' },
        { name: 'name', type: 'string' },
        { name: 'description', type: 'string' },
        { name: 'unitNo', type: 'string' },
        { name: 'address', type: 'string' },
        { name: 'type', type: 'string' },
        { name: 'propertyId', type: 'int' }
    ]
});
