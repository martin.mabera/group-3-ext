Ext.define('RentBooking.model.Property', {
    extend: 'Ext.data.Model',

    fields: [
        { name: 'id', type: 'int' },
        { name: 'name', type: 'string' },
        { name: 'description', type: 'string' },
        { name: 'bookingStatus', type: 'string' },
        { name: 'refCode', type: 'string' },
        { name: 'address', type: 'string' },
        { name: 'type', type: 'string' }
    ]
});
