Ext.define('RentBooking.model.Contract', {
    extend: 'Ext.data.Model',

    fields: [
        { name: 'id', type: 'int' },
        { name: 'contractCode', type: 'string' },
        { name: 'tenantId', type: 'int' },
        { name: 'rentAmount', type: 'number' },
        { name: 'startDate', type: 'date'},
        { name: 'endDate', type: 'date' },
        { name: 'frequency', type: 'string' },
        { name: 'contractStatus', type: 'string' }
    ]
});
