Ext.define('RentBooking.view.property.PropertyDetails', {
    extend: 'Ext.window.Window',
    xtype: 'propertydetails',

    requires: [
        'RentBooking.model.Property'
    ],

    viewModel: {
        type: 'propertyviewmodel'
    },

    bind: {
        title: 'Property Details - {selectedProperty.name}'
    },

    // controller: 'propertydetailscontroller',

    bodyPadding: 10,
    closable: true,
    autoShow: true,
    draggable: true,
    resizable: true,
    layout: 'fit',

    items: [{
        xtype: 'form',
        reference: 'form',
        layout: {
            type: 'vbox',
            align: 'center'
        },
        items: [{
            xtype: 'displayfield',
            fieldLabel: 'Name',
            bind: '{selectedProperty.name}',
            hidden: true
        }, {
            xtype: 'combobox',
            name: 'name',
            fieldLabel: 'Search for Tenant',
            anyMatch: true,
            allowBlank: true,
            editable: true,
            typeAhead: true,
            forceSelection: true,
            queryMode: 'remote', // Use 'remote' for remote data loading
            displayField: 'name',
            valueField: 'id',
            selectOnFocus: true,
            triggerAction: 'all',
            store: "tenants"
        },
            {
                xtype: 'displayfield',
                fieldLabel: 'Quantity',
                bind: '{selectedProperty.quantity}'
            }]
    }],

    buttons: [{
        text: 'Place Order',
        handler: 'onPlaceOrder'
    }, {
        text: 'Save',
        handler: 'onAddPropertySubmit'
    }, {
        text: 'Cancel',
        handler: 'onAddPropertyCancel'
    }]
});
  