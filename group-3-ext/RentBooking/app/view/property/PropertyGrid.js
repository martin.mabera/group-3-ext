Ext.define('RentBooking.view.property.PropertyGrid', {
    extend: 'Ext.grid.Panel',
    xtype: 'propertygrid',
    controller: "propertyviewcontroller",
    
    store: {
        type: 'property'
    },
    title: 'Properties Information',
    plugins: {
        cellediting: {
            clicksToEdit: 2,
        },
    },
  /*  selModel: {
        selType: "checkboxmodel",
        mode: "SINGLE",
    },*/
    selModel: {
        selType: 'checkboxmodel',
        checkOnly: false,
        showHeaderCheckbox: true
    },

    listeners: {
        cellclick: 'onGridCellItemClick'
    },
    tbar: {
        overflowHandler: "menu",
        items: [
            {
                text: "Create Contract",
                handler: "onShowpropertyDetails",
                bind: {
                    disabled: "{!propertygrid.selection}",
                },
            },
            {
                text: "Add property",
                handler: "onAddproperty",
            }, {
                text: "Update property",
                handler: "onUpdateproperty",
                bind: {
                    disabled: "{!propertygrid.selection}",
                },
            }, {
                text: "Show Units",
                handler: "onshowUnits",
                bind: {
                    disabled: "{!propertygrid.selection}",
                },
            },
            {
                text: "Delete property",
                handler: "onDeleteproperty",
                bind: {
                    disabled: "{!propertygrid.selection}",
                },
            },

        ],
    },
    columns: [
        { text: 'ID', dataIndex: 'id', flex: 1 },
        { text: 'Name', dataIndex: 'name', flex: 2 },
        { text: 'Description', dataIndex: 'description', flex: 3 },
        { text: 'Booking Status', dataIndex: 'bookingStatus', flex: 2 },
        { text: 'Reference Code', dataIndex: 'refCode', flex: 2 },
        { text: 'Address', dataIndex: 'address', flex: 3 },
        { text: 'Type', dataIndex: 'type', flex: 2 }
    ],

    height: 600,
    width: '100%',
    listeners: {
        cellclick: "onGridCellClick",
    },

});
