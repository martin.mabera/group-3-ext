Ext.define("RentBooking.view.property.PropertyGridController", {
    extend: "Ext.app.ViewController",
    alias: "controller.propertyviewcontroller",

    onShowpropertyDetails: function (button) {
        var grid = button.up("grid"); // Find the nearest grid panel
        var selectedRecords = grid.getSelection(); // Get selected records

        if (selectedRecords.length > 0) {
            var selectedRecord = selectedRecords[0]; // Assuming single selection
            var propertyDetailsWindow = Ext.create(
                "RentBooking.view.property.PropertyDetails",
                {
                    viewModel: {
                        data: {
                            selectedProperty: selectedRecord,
                        },
                    },
                }
            );

            propertyDetailsWindow.show();
        } else {
            Ext.Msg.alert("No Selection", "Please select a property.");
        }
    },

    onAddproperty: function () {
        Ext.create('RentBooking.view.property.AddProperty').show();
    },

    onAddPropertyCancel: function (button) {
        var window = button.up('window'); // Get reference to the window
        if (window) {
            window.close(); // Close the window
        }
    },
    onUpdateproperty: function () {
        var grid = this.getView();
        var selectedRecord = grid.getSelection()[0];

        if (selectedRecord) {
            Ext.create('RentBooking.view.property.UpdatePropertyWindow', {
                viewModel: {
                    data: {
                        selectedProperty: selectedRecord.getData() // Pass the selected property data to the window
                    }
                }
            }).show();
        }
    },
    onshowUnits: function (btn, e, eOpts) {
        let requestGrid = this.getView();
        let lowerPanel = Ext.ComponentQuery.query("downpanel")[0];
        let unitGrid = Ext.ComponentQuery.query("unitgrid")[0];

        var grid = btn.up("grid"); // Find the nearest grid panel
        var selectedRecords = grid.getSelection(); // Get selected records
        if (selectedRecords.length > 0) {
            // Get the selected record's ID
            let selectedId = selectedRecords[0].get('id');

            console.log(selectedId);

            // Pass the ID to the grid
            unitGrid.getStore().filterById(selectedId);

            unitGrid.getStore().reload();
            if (requestGrid.getHeight() === 600) {
                requestGrid.setHeight(350);
                lowerPanel.setHeight(450);
                btn.setText("Hide Units");
            } else {
                requestGrid.setHeight(600);
                lowerPanel.setHeight(0);
                btn.setText("Show Units");
            }

        } else {
            Ext.Msg.alert("No Selection", "Please select a property.");
        }

    },

    /* onSaveUpdateProperty: function () {
      var window = this.getView();
      var form = window.down('form');
      var values = form.getValues();

      Ext.Ajax.request({
        url: 'http://localhost:6060/api/propertys/' + values.id, // Update the URL with the appropriate endpoint for updating a property
        method: 'PUT', //  using PUT method for updating
        headers: {
          'Content-Type': 'application/json'
        },
        jsonData: values,
        success: function (response) {
          var responseData = Ext.decode(response.responseText);
          if (responseData.success) {
            Ext.Msg.alert('Success', 'Property updated successfully!');

            // Optionally, you can reload the property grid to reflect the changes
             // Reload the property store to reflect the changes
             Ext.getStore('propertys').reload();
            window.close(); // Close the update window after successful update
          } else {
            Ext.Msg.alert('Error', responseData.msg || 'Failed to update property.');
          }
        },
        failure: function (response) {
          Ext.Msg.alert('Error', 'Failed to update property.');
        }
      });
    }, */

    onDeleteproperty: function () {
        var grid = this.getView();
        var selectedRecord = grid.getSelection()[0];

        if (selectedRecord) {
            Ext.Msg.confirm('Delete', 'Are you sure you want to delete this property?', function (btn) {
                if (btn === 'yes') {
                    // Send a request to delete the property from the backend
                    Ext.Ajax.request({
                        url: 'http://localhost:8087/api/v1/properties/delete/' + selectedRecord.get('id'), // Update the URL with the appropriate endpoint for deleting a property
                        method: 'DELETE', // Use DELETE method for deletion
                        success: function (response) {
                            if (response.status === 200) {
                                Ext.Msg.alert('Success', 'Property deleted successfully!');
                                grid.getStore().remove(selectedRecord);
                            } else {
                                Ext.Msg.alert('Error', responseData.msg || 'Failed to delete property.');
                            }
                        },
                        failure: function (response) {
                            Ext.Msg.alert('Error', 'Failed to delete property.');
                        }
                    });
                }
            });
        }
    },
    onGridCellItemClick: function (grid, record, element, rowIndex, e, eOpts){
        let pId = record.get('id');
        if (pId > 0) {
            Ext.getStore('unit').reload({
            });
        }
    }

    onGridCellClick: function (
        grid,
        td,
        cellIndex,
        record,
        tr,
        rowIndex,
        e,
        eOpts
    ) {
        let logStore = Ext.ComponentQuery.query("unitgrid")[0];
        logStore.getStore().filterById(record.get("id"));

        logStore.getStore().reload();
        console.log(record.get("id"));
    },
});