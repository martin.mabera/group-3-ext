Ext.define('RentBooking.view.property.AddProperty', {
    extend: 'Ext.window.Window',
    xtype: 'addproperty',
    title: 'Add Property',
    controller:"addpropertycontroller",
    /*  modal: true, */
    width: 400,
    layout: 'fit',
    items: [{
        xtype: 'form',
        reference: 'addform',
        bodyPadding: 10,
        items: [{
            xtype: 'textfield',
            fieldLabel: 'Property Name',
            name: 'name',
            allowBlank: false
        },
            {
                xtype: 'textarea',
                fieldLabel: 'Property Description',
                name: 'description',
                allowBlank: false
            },{
                xtype: 'textfield',
                fieldLabel: 'Property Address',
                name: 'address',
                allowBlank: false
            },

            {
                xtype: 'combobox',
                fieldLabel: 'Property Type',
                name: 'propertyType',
                allowBlank: false,
                queryMode: 'local',
                displayField: 'name',
                valueField: 'id',
                store: {
                    fields: ['id', 'name'],
                    data: [
                        { id: 'apartment', name: 'Apartment' },
                        { id: 'house', name: 'House' }
                    ]
                }
            },

        ]
    }],
    buttons: [{
        text: 'Add',
        handler: 'onAddPropertySubmit'
    }, {
        text: 'Cancel',
        handler: 'onAddPropertyCancel'
    }]
});