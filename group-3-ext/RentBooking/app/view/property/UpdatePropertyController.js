Ext.define('RentBooking.view.tenant.UpdatePropertyController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.updatepropertycontroller',

    onUpdateproperty: function () {
        var grid = this.getView();
        var selectedRecord = grid.getSelection()[0];

        if (selectedRecord) {
            Ext.create('RentBooking.view.property.UpdateProperty', {
                viewModel: {
                    data: {
                        selectedProperty: selectedRecord.getData() // Pass the selected property data to the window
                    }
                }
            }).show();
        }
    },
});
