Ext.define("RentBooking.view.property.UpdatePropertyWindow", {
    extend: "Ext.window.Window",
    xtype: "updateproperty",
  
    controller: "updatepropertycontroller",
    title: "Update Property",
    width: 400,
    height: 300,
    layout: "fit",
    modal: true,
    closable: true,
  
    viewModel: {
      data: {
        selectedProperty: {}, // Initialize selectedRequest data
      },
    },
  
    // Other configurations for the window
  
    items: [
      {
        xtype: "form",
        reference: "updateform",
        layout: "form",
        items: [
            {
                xtype: "textfield",
                fieldLabel: "Property Name",
                bind: "{selectedProperty.name}", 
            },
          {
            xtype: "textfield",
            fieldLabel: "Property Description",
            bind: "{selectedProperty.description}", 
          },
          {
            xtype: "textfield",
            fieldLabel: 'Property Address',
            bind: "{selectedProperty.address}", 
          },
          {
            xtype: "textfield",
            fieldLabel: "Property Type",
            bind: "{selectedProperty.type}", 
          },
         
          
        ],
      },
    ],
  
    buttons: [
      {
        text: "Save",
        handler: "onSaveUpdateLog",
      },
      {
        text: "Cancel",
        handler: "onCancelUpdateLog",
      },
    ],
  });