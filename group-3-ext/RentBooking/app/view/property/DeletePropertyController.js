Ext.define('RentBooking.view.tenant.DeletePropertyController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.deletepropertycontroller',

    onDeleteProperty: function () {
        var grid = this.getView();
        var selectedRecord = grid.getSelection()[0]; // Assuming you want to get the first selected record
        if (selectedRecord) {
            Ext.Msg.confirm('Delete', 'Are you sure you want to delete this property?', function (btn) {
                if (btn === 'yes') {
                    Ext.Ajax.request({
                        url: 'http://localhost:8081/api/property/deleteProperty/' + selectedRecord.get('id'), // Update the URL with the appropriate endpoint for deleting a tenant
                        method: 'DELETE', // Use DELETE method for deletion
                        success: function (response) {
                            var responseData = Ext.decode(response.responseText);
                            if (responseData.success) {
                                Ext.Msg.alert('Success', 'Tenant deleted successfully!');
                                grid.getStore().remove(selectedRecord); // Remove the record from the frontend grid
                            } else {
                                Ext.Msg.alert('Error', responseData.msg || 'Failed to delete property.');
                            }
                        },
                        failure: function (response) {
                            Ext.Msg.alert('Error', 'Failed to delete property.');
                        }
                    });
                }
            });
        } else {
            Ext.Msg.alert('Error', 'No property selected to delete.');
        }
    }
});
