Ext.define('RentBooking.view.property.AddPropertyController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.addpropertycontroller',

    onAddPropertySubmit: function(button) {
        var form = button.up('window').down('form');
        var values = form.getValues();

        Ext.Ajax.request({
            url: 'http://localhost:8087/api/v1/properties/createProperty',
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            jsonData: values,
            success: function(response) {
                console.log(response.status);
                var responseData = Ext.decode(response.responseText);
                if (response.status >= 200 && response.status <= 210) {
                    Ext.Msg.alert('Success', 'Property added successfully!');


                  var propertygrid = Ext.ComponentQuery.query('propertygrid')[0];
                  var propertystore = propertygrid.getStore();
                  propertystore.reload();

                  form.close();


                } else {
                    Ext.Msg.alert('Error', responseData.msg || 'Failed to add property.');
                }
            },
            failure: function(response) {
                Ext.Msg.alert('Error', 'Failed to add property.');
            }
        });
    },
    onAddPropertyCancel: function () {
        this.getView().close();
    }
});
  