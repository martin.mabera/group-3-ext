Ext.define("RentBooking.view.unit.UnitGridController", {
    extend: "Ext.app.ViewController",
    alias: "controller.unitviewcontroller",

    // init: function (){
    //     let unitStore = Ext.getCmp('unitGrdId').getStore();
    //     unitStore.load();
    // },

    onShowunitDetails: function (button) {
        var grid = button.up("grid"); // Find the nearest grid panel
        var selectedRecords = grid.getSelection(); // Get selected records

        if (selectedRecords.length > 0) {
            var selectedRecord = selectedRecords[0]; // Assuming single selection
            var unitDetailsWindow = Ext.create(
                "RentBooking.view.unit.UnitDetails",
                {
                    viewModel: {
                        data: {
                            selectedUnit: selectedRecord,
                        },
                    },
                }
            );

            unitDetailsWindow.show();
        } else {
            Ext.Msg.alert("No Selection", "Please select a unit.");
        }
    },

    onAddunit: function() {
        Ext.create('RentBooking.view.unit.AddUnit').show();
    },

    onAddUnitCancel: function(button) {
        var window = button.up('window'); // Get reference to the window
        if (window) {
            window.close(); // Close the window
        }
    },
    onUpdateunit: function () {
        var grid = this.getView();
        var selectedRecord = grid.getSelection()[0];

        if (selectedRecord) {
            Ext.create('RentBooking.view.unit.UpdateUnit', {
                viewModel: {
                    data: {
                        selectedUnit: selectedRecord.getData() // Pass the selected unit data to the window
                    }
                }
            }).show();
        }
    },

    /* onSaveUpdateUnit: function () {
      var window = this.getView();
      var form = window.down('form');
      var values = form.getValues();

      Ext.Ajax.request({
        url: 'http://localhost:6060/api/units/' + values.id, // Update the URL with the appropriate endpoint for updating a unit
        method: 'PUT', //  using PUT method for updating
        headers: {
          'Content-Type': 'application/json'
        },
        jsonData: values,
        success: function (response) {
          var responseData = Ext.decode(response.responseText);
          if (responseData.success) {
            Ext.Msg.alert('Success', 'Unit updated successfully!');

            // Optionally, you can reload the unit grid to reflect the changes
             // Reload the unit store to reflect the changes
             Ext.getStore('units').reload();
            window.close(); // Close the update window after successful update
          } else {
            Ext.Msg.alert('Error', responseData.msg || 'Failed to update unit.');
          }
        },
        failure: function (response) {
          Ext.Msg.alert('Error', 'Failed to update unit.');
        }
      });
    }, */

    onDeleteunit: function () {
        var grid = this.getView();
        var selectedRecord = grid.getSelection()[0];

        if (selectedRecord) {
            Ext.Msg.confirm('Delete', 'Are you sure you want to delete this unit?', function (btn) {
                if (btn === 'yes') {
                    // Send a request to delete the unit from the backend
                    Ext.Ajax.request({
                        url: 'http://localhost:6060/api/units/' + selectedRecord.get('id'), // Update the URL with the appropriate endpoint for deleting a unit
                        method: 'DELETE', // Use DELETE method for deletion
                        success: function (response) {
                            var responseData = Ext.decode(response.responseText);
                            if (responseData.success) {
                                Ext.Msg.alert('Success', 'Unit deleted successfully!');

                                // Optionally, you can reload the unit grid to reflect the changes
                                grid.getStore().remove(selectedRecord); // Remove the record from the frontend grid
                            } else {
                                Ext.Msg.alert('Error', responseData.msg || 'Failed to delete unit.');
                            }
                        },
                        failure: function (response) {
                            Ext.Msg.alert('Error', 'Failed to delete unit.');
                        }
                    });
                }
            });
        }
    },


    /* onUnitGridCellClick: function (
      grid,
      td,
      cellIndex,
      record,
      tr,
      rowIndex,
      e,
      eOpts
    ) {
      let logStore = Ext.ComponentQuery.query("loggrid")[0].getStore();
      logStore.reload({
        params: {
          id: record.get("_id"),
        },
      });
      console.log(record.get("_id"));
    }, */
});