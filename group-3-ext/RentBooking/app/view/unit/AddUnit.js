Ext.define('RentBooking.view.unit.AddUnit', {
    extend: 'Ext.window.Window',
    xtype: 'addunit',
    title: 'Add unit',
    controller:"addunitcontroller",
    /*  modal: true, */
    width: 400,
    layout: 'fit',
    items: [{
        xtype: 'form',
        reference: 'addform',
        bodyPadding: 10,
        items: [{
            xtype: 'textfield',
            fieldLabel: 'Name',
            name: 'name'
        }, {
            xtype: 'textfield',
            fieldLabel: 'Description',
            name: 'description'
        }, {
            xtype: 'textfield',
            fieldLabel: 'Unit No',
            name: 'unitNo'
        }, {
            xtype: 'textfield',
            fieldLabel: 'Address',
            name: 'address'
        }, {
            xtype: 'textfield',
            fieldLabel: 'Type',
            name: 'type'
        },
        //     {
        //     xtype: 'combobox',
        //     fieldLabel: 'Property',
        //     name: 'propertyId',
        //     store: 'property',
        //     displayField: 'name',
        //     valueField: 'id',
        //     queryMode: 'local',
        //     forceSelection: true
        // }
        ],
    }],
    buttons: [{
        text: 'Add',
        handler: 'onAddunitSubmit'
    }, {
        text: 'Cancel',
        handler: 'onAddunitCancel'
    }]
});