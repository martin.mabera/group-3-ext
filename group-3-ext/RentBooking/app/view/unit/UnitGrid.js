Ext.define('RentBooking.view.unit.UnitGrid', {
    extend: 'Ext.grid.Panel',
    xtype: 'unitgrid',
    controller: "unitviewcontroller",

    store: {
        type: 'unit'
    },
    plugins: {
        cellediting: {
            clicksToEdit: 2,
        },
    },
    selModel: {
        selType: "checkboxmodel",
        mode: "SINGLE",
    },
    tbar: {
        overflowHandler: "menu",
        items: [
            {
                text: "Show unit Details",
                handler: "onShowunitDetails",
                bind: {
                    disabled: "{!unitsgridview.selection}",
                },
            },
            {
                text: "Add unit",
                handler: "onAddunit",
            }, {
                text: "Update unit",
                handler: "onUpdateunit",
                bind: {
                    disabled: "{!unitsgridview.selection}",
                },
            },
            {
                text: "Delete unit",
                handler: "onDeleteunit",
                bind: {
                    disabled: "{!unitsgridview.selection}",
                },
            },

        ],
    },
    columns: [
        { text: 'ID', dataIndex: 'id', flex: 1 },
        { text: 'Name', dataIndex: 'name', flex: 1 },
        { text: 'Description', dataIndex: 'description', flex: 1 },
        { text: 'Unit No', dataIndex: 'unitNo', flex: 1 },
        { text: 'Address', dataIndex: 'address', flex: 1 },
        { text: 'Type', dataIndex: 'type', flex: 1 },
        { text: 'Property ID', dataIndex: 'propertyId', flex: 1 }
    ],

    height: 400,
    width: '100%'
});
