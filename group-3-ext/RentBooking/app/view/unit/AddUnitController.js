Ext.define('RentBooking.view.unit.AddUnitController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.addunitcontroller',

    onAddunitSubmit: function(button) {
        var form = button.up('window').down('form');
        var values = form.getValues();

        Ext.Ajax.request({
            url: 'http://localhost:8081/api/unit/createUnit',
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            jsonData: values,
            success: function(response) {
                console.log(response.status);
                if (response.status >= 200 && response.status <= 210) {
                    Ext.Msg.alert('Success', 'Unit added successfully!');


                    var unitgrid = Ext.ComponentQuery.query('unitgrid')[0];
                    var unitstore = unitgrid.getStore();
                    unitstore.reload();
                    form.close();

                } else {
                    Ext.Msg.alert('Error', responseData.msg || 'Failed to add unit.');
                }
            },
            failure: function(response) {
                Ext.Msg.alert('Error', 'Failed to add unit.');
            }
        });
    },
    onAddUnitCancel: function () {
        this.getView().close();
    }
});
  