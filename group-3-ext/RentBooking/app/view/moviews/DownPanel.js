Ext.define('RentBooking.view.moviews.DownPanel',{
    extend:'Ext.tab.Panel',
    xtype:'downpanel',
    reference:"downpanel",
    height:0,
    scrollable: true,
    items: [{
        title: 'Units',
        items: [
            {
                xtype:'unitgrid'
            }
        ]
    }]
})