Ext.define('RentBooking.view.tenant.AddTenant', {
    extend: 'Ext.window.Window',
    xtype: 'addtenant',
    title: 'Add tenant',
    controller:"addtenantcontroller",
    /*  modal: true, */
    width: 400,
    layout: 'fit',
    items: [{
        xtype: 'form',
        reference: 'addform',
        bodyPadding: 10,
        items: [
            {
                xtype: 'textfield',
                fieldLabel: 'Tenant Name',
                name: 'tenantName',
                allowBlank: false
            },
            {
                xtype: 'textfield',
                fieldLabel: 'Tenant Email',
                name: 'tenantEmail',
                vtype: 'email'
            },
            {
                xtype: 'numberfield',
                fieldLabel: 'National ID',
                name: 'nationalId'
            },
            {
                xtype: 'textfield',
                fieldLabel: 'Phone Number',
                name: 'phoneNumber'
            },
            {
                xtype: 'textfield',
                fieldLabel: 'Account Name',
                name: 'accountName'
            },
            {
                xtype: 'numberfield',
                fieldLabel: 'Account Number',
                name: 'accountNumber'
            },
            {
                xtype: 'textfield',
                fieldLabel: 'Tax PIN',
                name: 'taxPin'
            },
            {
                xtype: 'combobox',
                fieldLabel: 'Deleted',
                name: 'deleted',
                store: ['Yes', 'No'],
                queryMode: 'local',
                editable: false,
                allowBlank: false,
                listeners: {
                    change: function(combo, newValue) {
                        combo.setValue(newValue.toUpperCase());
                    }
                }
            }
        ],
    }],
    buttons: [{
        text: 'Add',
        handler: 'onAddtenantSubmit'
    }, {
        text: 'Cancel',
        handler: 'onAddtenantCancel'
    }]
});