Ext.define('RentBooking.view.tenant.UpdateTenantController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.updatetenantcontroller',

    onUpdatetenant: function () {
        var grid = this.getView();
        var selectedRecord = grid.getSelection()[0];

        if (selectedRecord) {
            Ext.create('RentBooking.view.tenant.UpdateTenant', {
                viewModel: {
                    data: {
                        selectedTenant: selectedRecord.getData() // Pass the selected tenant data to the window
                    }
                }
            }).show();
        }
    },
});
