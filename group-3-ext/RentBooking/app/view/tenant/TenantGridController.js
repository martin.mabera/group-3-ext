Ext.define("RentBooking.view.tenant.TenantGridController", {
    extend: "Ext.app.ViewController",
    alias: "controller.tenantviewcontroller",


    onShowtenantDetails: function (button) {
        var grid = button.up("grid"); // Find the nearest grid panel
        var selectedRecords = grid.getSelection(); // Get selected records

        if (selectedRecords.length > 0) {
            var selectedRecord = selectedRecords[0]; // Assuming single selection
            var tenantDetailsWindow = Ext.create(
                "RentBooking.view.tenant.TenantDetails",
                {
                    viewModel: {
                        data: {
                            selectedTenant: selectedRecord,
                        },
                    },
                }
            );

            tenantDetailsWindow.show();
        } else {
            Ext.Msg.alert("No Selection", "Please select a tenant.");
        }
    },

    onAddtenant: function() {
        Ext.create('RentBooking.view.tenant.AddTenant').show();
    },

    onAddTenantCancel: function(button) {
        var window = button.up('window'); // Get reference to the window
        if (window) {
            window.close(); // Close the window
        }
    },
    onUpdatetenant: function () {
        var grid = this.getView();
        var selectedRecord = grid.getSelection()[0];

        if (selectedRecord) {
            Ext.create('RentBooking.view.property.UpdateTenantWindow', {
                viewModel: {
                    data: {
                        selectedTenant: selectedRecord.getData() // Pass the selected tenant data to the window
                    }
                }
            }).show();
        }
    },

    /* onSaveUpdateTenant: function () {
      var window = this.getView();
      var form = window.down('form');
      var values = form.getValues();

      Ext.Ajax.request({
        url: 'http://localhost:6060/api/tenants/' + values.id, // Update the URL with the appropriate endpoint for updating a tenant
        method: 'PUT', //  using PUT method for updating
        headers: {
          'Content-Type': 'application/json'
        },
        jsonData: values,
        success: function (response) {
          var responseData = Ext.decode(response.responseText);
          if (responseData.success) {
            Ext.Msg.alert('Success', 'Tenant updated successfully!');

            // Optionally, you can reload the tenant grid to reflect the changes
             // Reload the tenant store to reflect the changes
             Ext.getStore('tenants').reload();
            window.close(); // Close the update window after successful update
          } else {
            Ext.Msg.alert('Error', responseData.msg || 'Failed to update tenant.');
          }
        },
        failure: function (response) {
          Ext.Msg.alert('Error', 'Failed to update tenant.');
        }
      });
    }, */

    onDeletetenant: function () {
        var grid = this.getView();
        var selectedRecord = grid.getSelection()[0];

        if (selectedRecord) {
            Ext.Msg.confirm('Delete', 'Are you sure you want to delete this tenant?', function (btn) {
                if (btn === 'yes') {
                    // Send a request to delete the tenant from the backend
                    Ext.Ajax.request({
                        url: 'http://localhost:6060/api/tenants/' + selectedRecord.get('id'), // Update the URL with the appropriate endpoint for deleting a tenant
                        method: 'DELETE', // Use DELETE method for deletion
                        success: function (response) {
                            var responseData = Ext.decode(response.responseText);
                            if (responseData.success) {
                                Ext.Msg.alert('Success', 'Tenant deleted successfully!');

                                // Optionally, you can reload the tenant grid to reflect the changes
                                grid.getStore().remove(selectedRecord); // Remove the record from the frontend grid
                            } else {
                                Ext.Msg.alert('Error', responseData.msg || 'Failed to delete tenant.');
                            }
                        },
                        failure: function (response) {
                            Ext.Msg.alert('Error', 'Failed to delete tenant.');
                        }
                    });
                }
            });
        }
    },


    /* onTenantGridCellClick: function (
      grid,
      td,
      cellIndex,
      record,
      tr,
      rowIndex,
      e,
      eOpts
    ) {
      let logStore = Ext.ComponentQuery.query("loggrid")[0].getStore();
      logStore.reload({
        params: {
          id: record.get("_id"),
        },
      });
      console.log(record.get("_id"));
    }, */
});