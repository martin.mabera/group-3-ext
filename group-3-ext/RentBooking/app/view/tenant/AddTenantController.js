Ext.define('RentBooking.view.tenant.AddTenantController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.addtenantcontroller',

    onAddtenantSubmit: function(button) {
        var form = button.up('window').down('form');
        var values = form.getValues();

        Ext.Ajax.request({
            url: 'http://localhost:8081/api/tenant/createTenant',
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            jsonData: values,
            success: function(response) {
                console.log(response.status);
                if (response.status >= 200 && response.status <= 210) {
                    Ext.Msg.alert('Success', 'Tenant added successfully!');


                  var tenantgrid = Ext.ComponentQuery.query('tenantgrid')[0];
                  var tenantstore = tenantgrid.getStore();
                  tenantstore.reload();
                  form.close();

                } else {
                    Ext.Msg.alert('Error', responseData.msg || 'Failed to add tenant.');
                }
            },
            failure: function(response) {
                Ext.Msg.alert('Error', 'Failed to add tenant.');
            }
        });
    },
    onAddTenantCancel: function () {
        this.getView().close();
    }
});
  