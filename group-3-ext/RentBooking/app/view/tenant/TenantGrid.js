Ext.define("RentBooking.view.tenant.TenantGrid", {
    extend: "Ext.grid.Panel",
    xtype: "tenantgrid",
    id: "tenantGridId",

    controller: "tenantviewcontroller",

    store: {
        type: "tenants",
    },

    height: 600,
    title: "Tenants Information",
    plugins: {
        cellediting: {
            clicksToEdit: 2,
        },
    },
    selModel: {
        selType: "checkboxmodel",
        mode: "SINGLE",
    },

    tbar: {
        overflowHandler: "menu",
        items: [
            {
                text: "Show tenant Details",
                handler: "onShowtenantDetails",
                bind: {
                    disabled: "{!tenantsgridview.selection}"
                },
            },
            {
                text: "Add tenant",
                handler: "onAddtenant"
            },
            {
                text: "Update tenant",
                handler: "onUpdatetenant",
                bind: {
                    disabled: "{!tenantsgridview.selection}"
                }
            },
            {
                text: "Delete tenant",
                handler: "onDeleteTenant",
                bind: {
                    disabled: "{!tenantsgridview.selection}",
                },
            },


        ],
    },
    columns: [
        { text: 'ID', dataIndex: 'id', flex: 1 },
        { text: 'Tenant Name', dataIndex: 'tenantName', flex: 2 },
        { text: 'Email', dataIndex: 'tenantEmail', flex: 2 },
        { text: 'National ID', dataIndex: 'nationalId', flex: 2 },
        { text: 'Phone Number', dataIndex: 'phoneNumber', flex: 2 },
        { text: 'Account Name', dataIndex: 'accountName', flex: 2 },
        { text: 'Account Number', dataIndex: 'accountNumber', flex: 2 },
        { text: 'Tax PIN', dataIndex: 'taxPin', flex: 2 },
        { text: 'Deleted', dataIndex: 'deleted', flex: 1 }
    ],
    bbar: {
        xtype: "pagingtoolbar",
        displayInfo: true,
    },
    viewConfig: {
        scrollable: true,
    },
    /*  listeners: {
       cellclick: "ontenantGridCellClick",
     }, */
});