Ext.define('RentBooking.view.tenant.DeleteTenantController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.deletetenantcontroller',

    onDeleteTenant: function () {
        var grid = this.getView();
        var selectedRecord = grid.getSelection()[0]; // Assuming you want to get the first selected record
        if (selectedRecord) {
            Ext.Msg.confirm('Delete', 'Are you sure you want to delete this tenant?', function (btn) {
                if (btn === 'yes') {
                    Ext.Ajax.request({
                        url: 'http://localhost:8081/api/tenant/deleteTenant/' + selectedRecord.get('id'), // Update the URL with the appropriate endpoint for deleting a tenant
                        method: 'DELETE', // Use DELETE method for deletion
                        success: function (response) {
                            var responseData = Ext.decode(response.responseText);
                            if (responseData.success) {
                                Ext.Msg.alert('Success', 'Tenant deleted successfully!');
                                grid.getStore().remove(selectedRecord); // Remove the record from the frontend grid
                            } else {
                                Ext.Msg.alert('Error', responseData.msg || 'Failed to delete tenant.');
                            }
                        },
                        failure: function (response) {
                            Ext.Msg.alert('Error', 'Failed to delete tenant.');
                        }
                    });
                }
            });
        } else {
            Ext.Msg.alert('Error', 'No tenant selected to delete.');
        }
    }
});
