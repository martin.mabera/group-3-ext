Ext.define("RentBooking.view.property.UpdateTenantWindow", {
    extend: "Ext.window.Window",
    xtype: "updatetenant",
  
    controller: "updatetenantcontroller",
    title: "Update Tenant",
    width: 400,
    height: 300,
    layout: "fit",
    modal: true,
    closable: true,
  
    viewModel: {
      data: {
        selectedTenant: {}, // Initialize selectedRequest data
      },
    },
  
    // Other configurations for the window
  
    items: [
      {
        xtype: "form",
        reference: "updatetenantform",
        layout: "form",
        items: [
           
            {
              xtype: 'textfield',
              fieldLabel: 'Tenant Name',
              bind: "{selectedTenant.tenantName}", 
          },
          {
              xtype: 'textfield',
              fieldLabel: 'Tenant Email',
              bind: "{selectedTenant.tenantEmail}", 
          },
          {
              xtype: 'numberfield',
              fieldLabel: 'National ID',
              bind: "{selectedTenant.nationalId}", 
          },
          {
              xtype: 'textfield',
              fieldLabel: 'Phone Number',
              bind: "{selectedTenant.phoneNumber}", 
          },
          {
              xtype: 'textfield',
              fieldLabel: 'Account Name',
              bind: "{selectedTenant.accountName}", 
          },
          {
              xtype: 'numberfield',
              fieldLabel: 'Account Number',
              bind: "{selectedTenant.accountNumber}", 
          },
          {
              xtype: 'textfield',
              fieldLabel: 'Tax PIN',
              bind: "{selectedTenant.taxPin}", 
          },
          {
            xtype: 'combobox',
            fieldLabel: 'Deleted',
            name: 'deleted',
            store: ['Yes', 'No'],
            queryMode: 'local',
            editable: false,
            allowBlank: false,
            listeners: {
                change: function(combo, newValue) {
                    combo.setValue(newValue.toUpperCase());
                }
            }
        }
          
          
        ],
      },
    ],
  
    buttons: [
      {
        text: "Save",
        handler: "onSaveUpdateLog",
      },
      {
        text: "Cancel",
        handler: "onCancelUpdateLog",
      },
    ],
  });