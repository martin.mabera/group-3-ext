Ext.define('RentBooking.view.booking.BookingGrid', {
    extend: 'Ext.grid.Panel',
    xtype: 'bookinggrid',

    store: {
        type: 'booking'
    },
    title: 'Booking Information',
    height: 600,
    plugins: {
        cellediting: {
            clicksToEdit: 2,
        },
    },
    selModel: {
        selType: "checkboxmodel",
        mode: "SINGLE",
    },

    tbar: {
        overflowHandler: "menu",
        items: [
            {
                text: "Show contract Details",
                handler: "onShowcontractDetails",
                bind: {
                    disabled: "{!contractsgridview.selection}",
                },
            },
            {
                text: "Add contract",
                handler: "onAddcontract",
            }, {
                text: "Update contract",
                handler: "onUpdatecontract",
                bind: {
                    disabled: "{!contractsgridview.selection}",
                },
            },
            {
                text: "Delete contract",
                handler: "onDeletecontract",
                bind: {
                    disabled: "{!contractsgridview.selection}",
                },
            },

        ],
    },
    columns: [
        { text: 'ID', dataIndex: 'id', flex: 1 },
        { text: 'Booking Date', dataIndex: 'bookingDate', xtype: 'datecolumn', format: 'Y-m-d H:i:s', flex: 2 },
        { text: 'Description', dataIndex: 'bookingDescription', flex: 3 },
        { text: 'Contract ID', dataIndex: 'contractId', flex: 1 },
        { text: 'Month', dataIndex: 'month', flex: 2 },
        { text: 'Year', dataIndex: 'year', flex: 1 },
        { text: 'Property ID', dataIndex: 'propertyId', flex: 1 }
    ],
    bbar: {
        xtype: "pagingtoolbar",
        displayInfo: true,
    },
    viewConfig: {
        scrollable: true,
    },
    /*  listeners: {
       cellclick: "oncontractGridCellClick",
     }, */
});
