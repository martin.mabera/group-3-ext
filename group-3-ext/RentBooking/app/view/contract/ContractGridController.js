Ext.define("RentBooking.view.contract.ContractGridController", {
    extend: "Ext.app.ViewController",
    alias: "controller.contractviewcontroller",

    onShowcontractDetails: function (button) {
        var grid = button.up("grid"); // Find the nearest grid panel
        var selectedRecords = grid.getSelection(); // Get selected records

        if (selectedRecords.length > 0) {
            var selectedRecord = selectedRecords[0]; // Assuming single selection
            var contractDetailsWindow = Ext.create(
                "RentBooking.view.contract.ContractDetails",
                {
                    viewModel: {
                        data: {
                            selectedContract: selectedRecord,
                        },
                    },
                }
            );

            contractDetailsWindow.show();
        } else {
            Ext.Msg.alert("No Selection", "Please select a contract.");
        }
    },

    onAddcontract: function() {
        Ext.create('RentBooking.view.contract.AddContract').show();
    },

    onAddContractCancel: function(button) {
        var window = button.up('window'); // Get reference to the window
        if (window) {
            window.close(); // Close the window
        }
    },
    onUpdatecontract: function () {
        var grid = this.getView();
        var selectedRecord = grid.getSelection()[0];

        if (selectedRecord) {
            Ext.create('RentBooking.view.contract.UpdateContract', {
                viewModel: {
                    data: {
                        selectedContract: selectedRecord.getData() // Pass the selected contract data to the window
                    }
                }
            }).show();
        }
    },

    /* onSaveUpdateContract: function () {
      var window = this.getView();
      var form = window.down('form');
      var values = form.getValues();

      Ext.Ajax.request({
        url: 'http://localhost:6060/api/contracts/' + values.id, // Update the URL with the appropriate endpoint for updating a contract
        method: 'PUT', //  using PUT method for updating
        headers: {
          'Content-Type': 'application/json'
        },
        jsonData: values,
        success: function (response) {
          var responseData = Ext.decode(response.responseText);
          if (responseData.success) {
            Ext.Msg.alert('Success', 'Contract updated successfully!');

            // Optionally, you can reload the contract grid to reflect the changes
             // Reload the contract store to reflect the changes
             Ext.getStore('contracts').reload();
            window.close(); // Close the update window after successful update
          } else {
            Ext.Msg.alert('Error', responseData.msg || 'Failed to update contract.');
          }
        },
        failure: function (response) {
          Ext.Msg.alert('Error', 'Failed to update contract.');
        }
      });
    }, */

    onDeletecontract: function () {
        var grid = this.getView();
        var selectedRecord = grid.getSelection()[0];

        if (selectedRecord) {
            Ext.Msg.confirm('Delete', 'Are you sure you want to delete this contract?', function (btn) {
                if (btn === 'yes') {
                    // Send a request to delete the contract from the backend
                    Ext.Ajax.request({
                        url: 'http://localhost:8087/api/contracts/' + selectedRecord.get('id'), // Update the URL with the appropriate endpoint for deleting a contract
                        method: 'DELETE', // Use DELETE method for deletion
                        success: function (response) {
                            var responseData = Ext.decode(response.responseText);
                            if (responseData.success) {
                                Ext.Msg.alert('Success', 'Contract deleted successfully!');

                                // Optionally, you can reload the contract grid to reflect the changes
                                grid.getStore().remove(selectedRecord); // Remove the record from the frontend grid
                            } else {
                                Ext.Msg.alert('Error', responseData.msg || 'Failed to delete contract.');
                            }
                        },
                        failure: function (response) {
                            Ext.Msg.alert('Error', 'Failed to delete contract.');
                        }
                    });
                }
            });
        }
    },


    /* onContractGridCellClick: function (
      grid,
      td,
      cellIndex,
      record,
      tr,
      rowIndex,
      e,
      eOpts
    ) {
      let logStore = Ext.ComponentQuery.query("loggrid")[0].getStore();
      logStore.reload({
        params: {
          id: record.get("_id"),
        },
      });
      console.log(record.get("_id"));
    }, */
});