Ext.define('RentBooking.view.contract.AddContractController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.addcontractcontroller',

    onCreateContract: function(button) {
        var form = button.up('window').down('form');
        var values = form.getValues();
        console.log(values);
        let unitNo = values.unitNo;
        let nationalId = values.nationalId;

        console.log(unitNo);
        console.log(nationalId);
        let param={
            "unitNo":unitNo,
            "nationalId":nationalId
        }
        Ext.Ajax.request({
            url: 'http://localhost:8083/api/contracts/createContract',
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            jsonData: values,
            success: function(response) {
                var responseData = Ext.decode(response.responseText);
                if (responseData.success) {
                    Ext.Msg.alert('Success', 'Contract added successfully!');


                  var contractgrid = Ext.ComponentQuery.query('contractgrid')[0];
                  var contractstore = contractgrid.getStore();
                  contractstore.reload();

                  form.close();


                } else {
                    Ext.Msg.alert('Error', responseData.msg || 'Failed to add contract.');
                }
            },
            failure: function(response) {
                Ext.Msg.alert('Error', 'Failed to add contract.');
            }
        });
    },
    onClose: function () {
        this.getView().close();
    },
    updateEndDate: function (){

        let startDate = Ext.getCmp('contractFormId').getForm().findField('startDate').getValue(); // Replace 'startDate' with the actual field name

        let tenure = Ext.getCmp('contractFormId').getForm().findField('tenure').getValue(); // Replace 'tenure' with the actual field name

        tenure = parseInt(tenure);

        let startDateObj = new Date(startDate);

        let endDate = new Date(startDateObj.getFullYear(), startDateObj.getMonth() + tenure, startDateObj.getDate());

        Ext.getCmp('contractFormId').getForm().findField('endDate').setValue(endDate);
    },

    onCreateContractCode: function (){
        let unitNo = Ext.getCmp('contractFormId').getForm().findField('unitNo').getValue();
        let nationalId = Ext.getCmp('contractFormId').getForm().findField('nationalId').getValue();
        Ext.getCmp('contractFormId').getForm().findField('contractCode').setValue(unitNo+'/'+nationalId);
    }
});
  