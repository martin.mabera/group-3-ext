Ext.define('RentBooking.view.contract.AddContract', {
    extend: 'Ext.window.Window',
    xtype: 'addcontract',
    title: 'Add contract',
    controller:"addcontractcontroller",
    width: 400,
    items: [{
        xtype: 'form',
        id:'contractFormId',
        reference: 'addform',
        layout:'anchor',
        bodyPadding: 10,
        items: [
            {
                xtype:'textfield',
                fieldLabel:'UNIT NO',
                allowBlank:false,
                name:'unitNo',
                listeners: {
                    focusleave: 'onCreateContractCode'
                },
                anchor: '100%'
            },
            {
                xtype: 'textfield',
                fieldLabel: 'Tenant ID Number',
                allowBlank:false,
                name:'nationalId',
                listeners: {
                    focusleave: 'onCreateContractCode'
                },
                anchor: '100%'
            },
            {
                xtype: 'textfield',
                fieldLabel: 'Contrcat Code',
                allowBlank:false,
                readOnly: true,
                name:'contractCode',
                anchor: '100%'
            },
            {
                xtype: 'numberfield',
                fieldLabel: 'Rent Amount',
                allowBlank:false,
                name:'rentAmount',
                allowDecimal:true,
                anchor: '100%'
            },
            {
                xtype: 'datefield',
                allowBlank:false,
                fieldLabel: 'Contract Start Date',
                name:'startDate',
                maxValue: new Date(),
                anchor: '100%'
            },
            {
                xtype: 'textfield',
                allowBlank:false,
                fieldLabel: 'Tenure',
                name:'tenure',
                blankText:'Tenure in Months',
                id:'tenure',
                listeners:{
                    focusleave:'updateEndDate'
                },
                anchor: '100%'
            },
            {
                xtype: 'datefield',
                fieldLabel: 'Contract End Date',
                name:'endDate',
                id:'endDate',
                readOnly:true,
                anchor: '100%'
            },
            {

                xtype: 'combo',
                fieldLabel: 'RENT FREQUENCY',
                name: 'frequency',
                store: {
                    type: 'frequencyStore'
                },
                queryMode: 'local',
                displayField: 'name',
                valueField: 'id',
                triggerAction: 'all',
                typeAhead: true,
                forceSelection: true,
                allowBlank: false,
                anchor: '100%'
            }
        ],

    }],
    buttons: [{
        text: 'Create Contract',
        handler: 'onCreateContract'
    }, {
        text: 'Cancel Operation',
        handler: 'onClose'
    }]
});