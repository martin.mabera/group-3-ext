Ext.define("RentBooking.view.contract.ContractsGrid", {
    extend: "Ext.grid.Panel",
    xtype: "contractgrid",

    controller: "contractviewcontroller",

    store: {
        type: "contracts",
    },
    title: "Contracts",
    height: 600,
    plugins: {
        cellediting: {
            clicksToEdit: 2,
        },
    },
    selModel: {
        selType: 'checkboxmodel',
        checkOnly: false,
        showHeaderCheckbox: true
    },

    tbar: {
        overflowHandler: "menu",
        items: [

            {
                text: "Create Lease/Contract",
                handler: "onAddcontract",
            },
            {
                text: "Delete contract",
                handler: "onDeletecontract",
                bind: {
                    disabled: "{!contractsgridview.selection}",
                },
            },
            {
                text: "Show contract Details",
                handler: "onShowcontractDetails",
                bind: {
                    disabled: "{!contractsgridview.selection}",
                },
            }

        ],
    },
    columns: [
        { text: 'ID', dataIndex: 'id', flex: 1 },
        { text: 'Contract Code', dataIndex: 'contractCode', flex: 2 },
        { text: 'Tenant ID', dataIndex: 'tenantId', flex: 1 },
        { text: 'Rent Amount', dataIndex: 'rentAmount', xtype: 'numbercolumn', format: '$0,0.00', flex: 2 },
        { text: 'Start Date', dataIndex: 'startDate', xtype: 'datecolumn', format: 'Y-m-d', flex: 2 },
        { text: 'End Date', dataIndex: 'endDate', xtype: 'datecolumn', format: 'Y-m-d', flex: 2 },
        { text: 'Frequency', dataIndex: 'frequency', flex: 2 },
        { text: 'Contract Status', dataIndex: 'contractStatus', flex: 2 }
    ],
    bbar: {
        xtype: "pagingtoolbar",
        displayInfo: true,
    },
    viewConfig: {
        scrollable: true,
    },
    /*  listeners: {
       cellclick: "oncontractGridCellClick",
     }, */
});