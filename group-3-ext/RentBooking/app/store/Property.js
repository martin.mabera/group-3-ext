Ext.define('RentBooking.store.Property', {
    extend: 'Ext.data.Store',
    alias: "store.property",
    model: 'RentBooking.model.Property',
    autoLoad: true,

    proxy: {
        type: 'ajax',
        url: 'http://localhost:8087/api/v1/properties/getProperties',
        reader: {
            type: 'json',
            rootProperty: 'data'
        },
    },

});
