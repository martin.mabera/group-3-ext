Ext.define('ReentBooking.store.common.FrequencyStore', {
    extend: 'Ext.data.Store',
    alias: 'store.frequencyStore',
    storeId: 'frequencyStore',
    fields: ['id', 'name'],
    data: [
        ['MONTHLY', 'Monthly'],
        ['QUARTERLY', 'Quarterly'],
        ['HALF_YEARLY', 'Half YearLy'],
        ['ANNUALLY', 'Annually']
    ]
});