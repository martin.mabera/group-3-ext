Ext.define('RentBooking.store.Units', {
    extend: 'Ext.data.Store',
    alias: "store.unit",
    model: 'RentBooking.model.Unit',
    autoLoad: true,

    proxy: {
        type: 'ajax',
        url: 'http://localhost:8087/api/v1/units/getUnits',
        reader: {
            type: 'json',
            rootProperty: 'data'
        }
    },
    // Custom function to filter units by ID
    filterById: function(unitId) {
        this.clearFilter(); // Clear any existing filters
        this.filterBy(function(record) {
            return record.get('id') === unitId;
        });
    }
});
