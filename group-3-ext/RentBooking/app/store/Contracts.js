Ext.define('RentBooking.store.Contracts', {
    extend: 'Ext.data.Store',
    model: 'RentBooking.model.Contract',
    alias: "store.contracts",
    autoLoad: true,

    proxy: {
        type: 'ajax',
        url: 'http//:localhost:8083/api/contracts/getRentBookingData', // Replace with actual backend API endpoint
        reader: {
            type: 'json',
            rootProperty: 'data'
        }
    }
});
