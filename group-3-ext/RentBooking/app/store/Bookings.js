Ext.define('RentBooking.store.Bookings', {
    extend: 'Ext.data.Store',
    model: 'RentBooking.model.Booking',
    alias: "store.booking",
    autoLoad: true,

    proxy: {
        type: 'ajax',
        url: 'path_to_backend_api/getBookingData',
        reader: {
            type: 'json',
            rootProperty: 'data'
        }
    }
});
