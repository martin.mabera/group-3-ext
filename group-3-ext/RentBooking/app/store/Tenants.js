Ext.define('RentBooking.store.Tenants', {
    extend: 'Ext.data.Store',
    alias: "store.tenants",
    storeId: "tenants",
    model: 'RentBooking.model.Tenant',
    autoLoad: true,

    proxy: {
        type: 'ajax',
        url: 'http://localhost:8081/api/tenant/getTenants',
        reader: {
            type: 'json',
            rootProperty: 'data'
        }
    },
});
